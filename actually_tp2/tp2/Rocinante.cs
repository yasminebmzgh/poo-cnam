﻿using System;
using System.Collections.Generic;

namespace tp2
{
    public class Rocinante : Vaisseau
    {
        public Rocinante()
        {
            this.m_ptStructureMax = 3;
            this.valeurActuelleStructure = 3;
            this.valeurActuelleBouclier = 5;
            this.m_ptBouclierMax = 5;

            this.m_armes = new List<Arme>();

            this.m_armes.Add(new Arme("Torpille", 3, 3, 2, 2));
        }

        public override void Attaque(Vaisseau v)
        {
            v.repartirPointsDegats(m_armes[0].SimulerTir());
        }
    }
}
