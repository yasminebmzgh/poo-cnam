﻿using System;
using System.Collections.Generic;

namespace tp2
{
    public abstract class Vaisseau
    {
        protected int m_ptStructureMax;
        protected int m_ptBouclierMax;
        protected List<Arme> m_armes;
        protected Armurerie armurerie;
        protected int valeurActuelleBouclier;
        protected int valeurActuelleStructure;


        public Vaisseau()
        {

        }
        public void SetPtStructureMax(int ptStructureMax)
        {
            m_ptStructureMax = ptStructureMax;
        }

        //Point de bouclier max
        public float GetPtBouclierMax()
        {
            if (m_ptStructureMax == 0)
            {
                return m_ptBouclierMax;
            }
            else
            {
                return 0;
            }
        }
        public void SetPtBouclierMax(int ptBouclierMax)
        {
            m_ptBouclierMax = ptBouclierMax;
        }

        //Liste d'arme
        public List<Arme> GetArmes()
        {
            return m_armes;
        }

        public void SetArmes(List<Arme> armes)
        {
            m_armes = armes;
        }
        public float getValeurActuelleBouclier()
        {
            return valeurActuelleBouclier;
        }

        public void setValeurActuelleBouclier(int valeur)
        {
            this.valeurActuelleBouclier = valeur;
        }

        public float getValeurActuelleStructure()
        {
            return valeurActuelleStructure;
        }
        public void setValeurActuelleStructure(int val)
        {
            this.valeurActuelleStructure = val;
        }

        public void AjouterArme(Arme armes)
        {
            if (armurerie.contientArme(armes))
            {
                if (m_armes.Count < 3)
                {
                    m_armes.Add(armes);
                }
                else
                {
                    Console.WriteLine("Le vaisseau ne peut stocker que trois armes.");
                }
            }
            else
            {
                throw new ArmurerieException();
            }
        }

        public void SupprimerArme(Arme armes)
        {
            m_armes.Remove(armes);
        }

        public void AfficherArme()
        {
            foreach (Arme arme in m_armes)
            {
                Console.WriteLine(arme.ToString());
            }
        }

        //Total de dégâts moyens infligés par le vaisseau
        public float DegatsMoyen()
        {
            float somme = 0;
            foreach (Arme arme in m_armes)
            {
                somme += (arme.GetDegatMin() + arme.GetDegatMax()) / 2;
            }

            return somme;
        }

        public void repartirPointsDegats(int degats)
        {
            if (valeurActuelleBouclier >= degats)
            {
                valeurActuelleBouclier -= degats;
            }
            else
            {
                degats -= valeurActuelleBouclier;
                valeurActuelleBouclier = 0;

                if (degats > valeurActuelleStructure)
                {
                    valeurActuelleStructure = 0;
                }
                else
                {
                    valeurActuelleStructure -= degats;
                }
            }
        }
        public abstract void Attaque(Vaisseau v);
    }
}
