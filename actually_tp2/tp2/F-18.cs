﻿using System;
using System.Collections.Generic;

namespace tp2
{
    public class F_18 : Vaisseau, IAptitude
    {
        public F_18()
        {
            this.m_ptStructureMax = 15;
            this.valeurActuelleStructure = 15;
            this.valeurActuelleBouclier = 0;
            this.m_ptBouclierMax = 0;

            this.m_armes = new List<Arme>();
        }

        public override void Attaque(Vaisseau v)
        {
            v.repartirPointsDegats(0);
        }

        public void Utiliser(List<Vaisseau> Vaisseaux)
        {
            double nbDegats = 0;

            if (Vaisseaux.IndexOf(this) == 0)
            {
                nbDegats = 10;

            }
        }
    }
}
