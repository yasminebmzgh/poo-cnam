﻿using System;
using System.Collections.Generic;

namespace tp2
{
    public class Tardis : Vaisseau, IAptitude
    {
        public Tardis()
        {
            this.m_ptStructureMax = 1;
            this.valeurActuelleStructure = 1;
            this.valeurActuelleBouclier = 0;
            this.m_ptBouclierMax = 0;

            this.m_armes = new List<Arme>();
        }

        public override void Attaque(Vaisseau v)
        {
            v.repartirPointsDegats(0);
        }

        public void Utiliser(List<Vaisseau> Vaisseaux)
        {
            Random vaisseau1 = new Random();
            Random vaisseau2 = new Random();

            int deplacer1 = vaisseau1.Next(0, Vaisseaux.Count);
            int deplacer2 = vaisseau2.Next(0, Vaisseaux.Count);

            Vaisseau v1 = Vaisseaux[deplacer1];
            Vaisseau v2 = Vaisseaux[deplacer2];

            Vaisseaux[deplacer1] = v2;
            Vaisseaux[deplacer2] = v1;
        }
    }
}
