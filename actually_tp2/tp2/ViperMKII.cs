﻿using System;
using System.Collections.Generic;

namespace tp2
{
    public class ViperMKII : Vaisseau
    {
        public ViperMKII()
        {
            this.m_ptStructureMax = 10;
            this.valeurActuelleStructure = 10;
            this.valeurActuelleBouclier = 15;
            this.m_ptBouclierMax = 15;

            this.m_armes = new List<Arme>();

            this.m_armes.Add(new Arme("Mitrailleuse", 2, 3, 0, 1));
            this.m_armes.Add(new Arme("EMG", 1, 7, 1, 1.5));
            this.m_armes.Add(new Arme("Missile", 4, 100, 2, 4));
        }

        public override void Attaque(Vaisseau v)
        {
            Random alea = new Random();
            int choix_arme = alea.Next(0, m_armes.Count);
            Arme arme = m_armes[choix_arme];

            arme.SetCompteur(0);
            v.repartirPointsDegats(arme.SimulerTir());
        }
    }
}
