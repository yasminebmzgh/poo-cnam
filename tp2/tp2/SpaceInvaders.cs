﻿using System;
using System.Collections.Generic;

namespace tp2
{
    public class SpaceInvaders
    {
        
        private static Joueur[] _players;



        public SpaceInvaders()
        {
        }

        private static void Init()
        {

            Joueur j1 = new Joueur("yasminebmzgh", "Yasmine", "Boumazzough");
            Console.WriteLine(j1.ToString());
            Joueur j2 = new Joueur("juliaro", "Julia", "Rohet");
            Console.WriteLine(j2.ToString());
            Joueur j3 = new Joueur("halamalek", "Hala", "Malek");
            Console.WriteLine(j3.ToString());
        }


        static void Main(string[] args)
        {
            List<Arme> armes = new List<Arme>();
            armes.Add(new Arme("Arme 1", 10, 40, 0, 1));
            armes.Add(new Arme("Arme 2", 20, 50, 1, 1));
            armes.Add(new Arme("Arme 3", 30, 70, 2, 1));

            Armurerie armurie = new Armurerie();

            armurie.toString();

            SpaceInvaders.Init();


        }
    }
}
