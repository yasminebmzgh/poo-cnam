﻿using System;
namespace tp2
{
    public class Arme
    {
        private string m_nomArme;
        private int m_degatMin;
        private int m_degatMax;
        private Type m_typeArme;
        private double m_temps_rechargement;
        private double m_compteur;

        public enum Type
        {
            Direct,
            Explosif,
            Guidé
        }

        public Arme(string nomArme, int degatMin, int degatMax, int type, double temps_rechargement)
        {
            this.m_nomArme = nomArme;
            this.m_degatMin = degatMin;
            this.m_degatMax = degatMax;
            this.m_typeArme = (Type)type;
            this.m_temps_rechargement = temps_rechargement;
            this.m_compteur = temps_rechargement;
        }
        public string GetNomArme()
        {
            return m_nomArme;
        }

        public int GetDegatMin()
        {
            return m_degatMin;
        }

        public int GetDegatMax()
        {
            return m_degatMax;
        }


        public void SetNomArme(string nomArme)
        {
            m_nomArme = nomArme;
        }

        public void SetDegatMin(int degatMin)
        {
            m_degatMin = degatMin;
        }

        public void SetDegatMax(int degatMax)
        {
            m_degatMax = degatMax;
        }

        public Type GetType()
        {
            return m_typeArme;
        }

        public double GetCompteur()
        {
            return m_compteur;
        }

        public void SetCompteur(double compteur)
        {
            m_compteur = compteur;
        }
        public override string ToString() => $"{m_nomArme} {{ {m_degatMin}, {m_degatMax}, {m_typeArme} }}";

    }
}
