﻿using System;
using System.Collections.Generic;

namespace tp2
{
    public class Armurerie
    {
        private List<Arme> m_armes = new List<Arme>();

        public Armurerie()
        {
            this.Init();
        }

        private void Init()
        {
            m_armes.Add(new Arme("HK21", 200, 300, 0, 1));
            m_armes.Add(new Arme("Redeye", 2000, 4000, 1, 1));
            m_armes.Add(new Arme("BarretteM95", 140, 560, 2, 1));
        }
        public List<Arme> GetListArme()
        {
            return m_armes;
        }

        public void SetListArme(List<Arme> listArme)
        {
            m_armes = listArme;
        }
        public bool contientArme(Arme a)
        {
            return m_armes.Contains(a);
        }
        public void AjouterArme(Arme arme)
        {
            m_armes.Add(arme);
        }

        public void SupprimerArme(Arme arme)
        {
            m_armes.Remove(arme);
        }


        public void toString()
        {
            foreach (Arme a in m_armes)
            {
                Console.WriteLine(a.ToString());
            }
        }
    }
}
