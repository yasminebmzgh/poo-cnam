﻿using System;
namespace tp2
{
    public class Joueur
    {
        private string _nom;
        private string _prenom;
        private string _pseudo;

        public Joueur(string nom, string prenom, string pseudo)
        {
            this._nom = FormaterNomPrenom(nom);
            this._prenom = FormaterNomPrenom(prenom);
            this._pseudo = pseudo;
        }
        public string GetNom()
        {
            return _nom;
        }

        public string GetPrenom()
        {
            return _prenom;
        }

        public string GetPseudo()
        {
            return _pseudo;
        }

        public void SetPseudo(string pseudo)
        {
            _pseudo = pseudo;
        }
        private static string FormaterNomPrenom(string nom)
        {
            return nom[0].ToString().ToUpper() + nom.Substring(1, nom.Length - 1).ToLower();
        }
        private string Fullname(string nom,string prenom)
        {
            return prenom + "" + nom;
        }
        public override string ToString()
        {
            return "Le joueur : " + _pseudo +"     "+ _prenom + " " + _nom ;
        }
        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Joueur j = obj as Joueur;
                if (_pseudo == j._pseudo)
                {
                    return false;
                }
                return true;
            }
        }


    }
}
