﻿using System;
namespace ConsoleGame
{
    public class MenuJoueur
    {
        public MenuJoueur()
        {
        }
        public void Display()
        {
            int choice;
            Console.WriteLine("");
            Console.WriteLine("=========== Menu Joueur ===========");
            Console.WriteLine("1. Creer un joueur ");
            Console.WriteLine("2. Supprimer un joueur ");
            while (!int.TryParse(Console.ReadLine(), out choice) || !new[] { 1, 2, 3 }.Contains(choice))
            {
                Console.WriteLine("Nombre incorrect, réessayer");
            }
            switch (choice)
            {
                case 1:
                    CreerJoueur();
                    break;
                case 2:
                    SupprimerJoueur();
                    break;

            }
        }
        public void CreerJoueur()
        {

        }
        public void SupprimerJoueur()
        {

        }
    }
}
