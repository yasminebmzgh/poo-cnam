﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SpaceInvadersArmory
{
    public class ArmeImporteur
    {
        public ArmeImporteur()
        {
        }
        public static Dictionary<string, int> ReadFromText(string path, int minimumLength, List<string> blacklist)
        {
            Dictionary<string, int> keyValuePairs = new Dictionary<string, int>();
            try
            {
                string[] arrayString = File.ReadAllText(path).Split(' ');
                foreach (string word in arrayString)
                {
                    string wordTypo = word.ToLower().Replace("\\p{P}+", "");
                    if (wordTypo.Length >= minimumLength && !blacklist.Contains(wordTypo))
                    {
                        if (keyValuePairs.ContainsKey(wordTypo)) keyValuePairs[wordTypo]++;
                        else keyValuePairs.Add(wordTypo, 1);
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("Le Fichier n'existe pas.");
            }
            return keyValuePairs;
        }

        public static void importFromFile(string filename)
        {

            string[] lines = System.IO.File.ReadAllLines(filename);
            foreach (string line in lines)
            {
                string[] words = line.Split(' ');
                EWeaponType type;
                switch (words[1])
                {
                    case "Direct":
                        type = EWeaponType.Direct;
                        break;
                    case "Explosif":
                        type = EWeaponType.Explosive;
                        break;
                    case "Guided":
                        type = EWeaponType.Guided;
                        break;
                    default:
                        type = EWeaponType.Direct;
                        break;
                }
                Armory.CreatBlueprint(words[0], type, Double.Parse(words[2]), Double.Parse(words[3]));
            }

        }
    }
}
